# About this repository
The main purpose of this repository is to proivde devlopment images that you can use for 
java or node development with jboss, mongo or mysql.

The images are based on base centos 6 image with following utilities pre-installed:

> make, gcc, gcc-c++, sudo, curl, wget, git, unzip, tar

# Supported tags and respective `Dockerfile` links
 - [`latest` (centos/Dockerfile)](https://bitbucket.org/vkkotha/docker-dev/src/e0f07814bfcde1fcc1f37bc4723bbbd924e27096/centos/Dockerfile?at=master)
 - [`jdk7` (jdk7/Dockerfile)](https://bitbucket.org/vkkotha/docker-dev/src/e0f07814bfcde1fcc1f37bc4723bbbd924e27096/jdk7/Dockerfile?at=master)
 - [`node` (node/Dockerfile)](https://bitbucket.org/vkkotha/docker-dev/src/e0f07814bfcde1fcc1f37bc4723bbbd924e27096/node/Dockerfile?at=master)
 - [`mongo` (mongo/Dockerfile)](https://bitbucket.org/vkkotha/docker-dev/src/e0f07814bfcde1fcc1f37bc4723bbbd924e27096/mongo/Dockerfile?at=master)
 - [`mysql` (mysql/Dockerfile)](https://bitbucket.org/vkkotha/docker-dev/src/e0f07814bfcde1fcc1f37bc4723bbbd924e27096/mysql/Dockerfile?at=master)
 - [`jboss6` (jboss6/Dockerfile)](https://bitbucket.org/vkkotha/docker-dev/src/e0f07814bfcde1fcc1f37bc4723bbbd924e27096/jboss6/Dockerfile?at=master)
 - [`apache` (apache/Dockerfile)](https://bitbucket.org/vkkotha/docker-dev/src/e0f07814bfcde1fcc1f37bc4723bbbd924e27096/apache/Dockerfile?at=master)

# Installation
Install dokcer Or boot2docker depending on your platform.
If you are using boot2docker run following to setup docker shell

	$ boot2docker start
	$ $(boot2docker shellinit)

After this you can install on the tagged images available in this repository.

To Install base centos image with the above tools use

	$ docker pull vkkotha/centos

To Install other development images like jdk7, node etc use the tags.

	$ docker pull vkkotha/centos:jdk7
	$ docker pull vkkotha/centos:node
	$ docker pull vkkotha/centos:mongo
	$ docker pull vkkotha/centos:mysql
	$ docker pull vkkotha/centos:apache

# Usage
Images in this repository can be used different ways. You can use base os image to use some unix tools like curl etc, or other development related images like jdk7, node etc.

When using the images you have to use vkkotha/centos:<tag>.
However to shorten the frequently used images you can tag them with a shorter name in your local docker registry with following

	$ docker tag vkkotha/centos:node node

The above will create alias for `vkkotha/centos:node` as `node`

## centos
This will start a base centos image with bash shell

	$ docker run -it --rm vkkotha/centos

This will execute curl command

	$ docker run -it --rm vkkotha/centos curl http://somesite.com

## jdk7
Following will start java and remove the container after exit

	$ docker run -it --rm vkkotha/centos:jdk7 java -version

## node
Following will start node and remove the container after exit

	$ docker run -it --rm vkkotha/centos:node node --version

To use this in a node project
 
 - Define a Dockerfile in you project folder with following, to create a node application image with global node packages.
	
		FROM vkkotha/node
		RUN npm install -g strongloop
	
 - Create an image from the current folder, and use that image for subsequent node application development.

		$ docker build --rm=true -t nodeapp .

 - After this you can run the node server like this

	 	$ docker run -it --rm nodeapp node app.js

## mongo
Following will start mongo server 

	$ docker run -d --name mongo vkkotha/centos:mongo

If you want to mount you host directory as data directory then use

	$ docker run -d --name mongo -v "$(pwd)"/data/mongo:/data vkkotha/centos:mongo

######Note: 
The mounting won't work when you do virutal box (boot2docker) host volume mapping as mongo doesn't support that file system. To work around this you can mount a docker data volume and use it.

To start a docker data container use following commaind

	$ docker create -v /data --name data vkkotha/centos

You can connect to data container using following, then you can create a mongo db directory inside the container like this

	$ docker run -it --rm --volumes-from data vkkotha/centos bash
	# mkdir /data/db

Now you can use the above directory when you start your mongo server like this

	$ docker run -d --name mongo --volumes-from data vkkotha/centos:mongo --smallfiles --dbpath /data/db

If you want to use mongo shell use

	$ docker exec -it mongo mongo

## mysql
Following will start mysql server 

	$ docker run -d --name mysql -e MYSQL_ROOT_PASSWORD=passwd vkkotha/centos:mysql

If you want to create a DB when starting up use

	$ docker run -d --name mysql -e MYSQL_ROOT_PASSWORD=passwd -e MYSQL_DATABASE=app1 -e MSQL_USER=app1 -e MYSQL_PASSWORD=passwd vkkotha/centos:mysql

If you want to mount you host directory as data directory then use

	$ docker run -d --name mysql -e MYSQL_ROOT_PASSWORD=passwd -v "$(pwd)"/data/mysql/db:/db vkkotha/centos:mysql

If you need to connect through mysql client you can use. This will put you in mysql command shell.

	$ docker exec -it mysql mysql --user=root --password=passwd

If you want to run your own sql script after db is started, run the DB server with following. You could also goto myql shell and execute script file by running *source /data/test.sql'*

#####SERVER

	$ docker run -d --name mysql -e MYSQL_ROOT_PASSWORD=passwd -v "$(pwd)"/data/mysql/db:/db -v "$(pwd)"/data/mysql:/data vkkotha/centos:mysql

#####CLIENT

	$ docker exec -it mysql mysql --user=root --password=passwd -e "source /data/test.sql"

## jboss
Jboss EAP official downloads are avialable at [jboss6.1-eap](https://www.jboss.org/download-manager/file/jboss-eap-6.1.0.GA.zip). Download the zip and save it in Jboss directory and then you can build the jboss docker image with following command

	$ cd docker-dev
	$ docker build --rm=true -t jboss6 ./jboss6

Once the images is built you can start a jboss container with

	$ docker run -d --name jboss6 -p 8080:8080 -p 9090:9090 jboss6

This will run jboss 6 on port 8080 and management console on 9090

# Supported Docker versions

This image is officially supported on Docker version 1.4.1.

Support for older versions (down to 1.0) is provided on a best-effort basis.

